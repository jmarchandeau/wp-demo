<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'slicedbread');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7zC/;ib**}lP,R1}W/f:t}]pI72ZN[P&z~7Fo{:*??AgX%;{?s4Lu`!}/h~X9j`H');
define('SECURE_AUTH_KEY',  'v8qW(tn3>QP:C$z,,2 hnAJEq&61.JVupg;Lt^]dTb4Jy[GBEh3V(jV<Euu>oZwy');
define('LOGGED_IN_KEY',    'Qm.^IhZ7xuBlW/$[yN(`j,-y!4Fq|]r9d<Jhu-h_eH~FK/_]XmrD>Q)a>Xk~45T:');
define('NONCE_KEY',        'kl#-w#GfrZKV^m(%.bf+/8Hoo+8y{ (alCrqbVnK2sxKuN;h]X:)So@)d/Y{97QX');
define('AUTH_SALT',        'A(}_B*!nl(OX}fyAk_,]t/`Tp^ ,Z=eqdE{k~7Z>Tn|G&A1#h=Y(nHU5~!4>|KD2');
define('SECURE_AUTH_SALT', '2sq)OU}kw9l6M15GJqwGRYrg#+<O2(U]6K8kJN}}A9Bd$*Ml$eNA[;z?Z4y&~{dH');
define('LOGGED_IN_SALT',   '.zaRR7!}T/Qh3.9X/5M*-E>V$T9+D%}/-$/6;Gm$UQB|g5)C[gFc{m(mJU1&gEKr');
define('NONCE_SALT',       'X$1fdJjr8fI/9h%UUFE]%&PaZ9g>KGdL_.q<mN{%Tt4d4@Q_.{Hzdi(zozk%7JTM');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
